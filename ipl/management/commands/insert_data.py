from django.core.management.base import BaseCommand
# from datetime import datetime
from ipl.models import matches
from django.db.models import Count
from ipl_project.project_code import database

def insert_data():
     database.open_file1()
     database.open_file2()

# def custom_command():
#      matches_played_per_year_Qset=matches.objects.values("season").annotate(number_of_matches=Count('season')).order_by('number_of_matches')
#      for i in matches_played_per_year_Qset:
#          print(i)

class Command(BaseCommand):
    def handle(self, **options):
        insert_data()
        # createData()