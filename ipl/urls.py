from django.conf.urls  import url
from . import views

urlpatterns = [
   url('/view', views.open,name='open'),
   url('/first', views.f_open,name='open'),
   url('/second', views.second_open,name='open'),
   url('/third', views.third_open,name='open'),   
   url('/forth', views.forth_open,name='open'),
   url('/story', views.story_open,name='open')
]