# from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render

from ipl_project.project_code import database,first_pro_logic,extra_run_calc_logic,top_economical_bowler_logic
from ipl_project.project_code import stacked_bar,story
def open(request):
   return HttpResponse("samrat")

def f_open(request):
    
    x,y=first_pro_logic.first_logic()
  
#     match_series = {
#        'name': 'Matches',
#        'data': matchList,
#        'color': 'green'
#    }
    chart = {
       'chart': {'type': 'column'},
       'title': {'text': 'Matches played per year'},
       'xAxis':{'categories': x},#'name':'something'},
       'yAxis': {'title': {
                       'text': 'Matches', 
                    }},
       
       'series':[{'name':'Year','data':y,'color':'orange'}]
    
    }

    return render(request,'ipl/index.html',context={'chart':chart})

def third_open(request):
   
    x,y=extra_run_calc_logic.code_logic()

    chart = {
       'chart': {'type': 'column'},
       'title': {'text': 'Extra runs conceded per team in 2016'},
       'xAxis':{'categories': x},#'name':'something'},
       'yAxis': {'title': {
                       'text': 'Extra Runs',
                    #    'y': -40
                    }},
       'series':[{'name':'Team','data':y}]
    
    }
    return render(request,'ipl/index.html',context={'chart':chart})

def forth_open(request):
    x,y=top_economical_bowler_logic.code_logic()

    chart = {
       'chart': {'type': 'column'},
       'title': {'text': ' Top economical bowlers in 2015'},
       'xAxis':{'categories': x},#'name':'jhwhfdsomething'},
       'yAxis': {'title': {
                       'text': 'Runs',
                       'y': -10
                    }},
       'series':[{'name':'Bowler','data':y,'color':'red'}]
    
    }
    return render(request,'ipl/index.html',context={'chart':chart})
def second_open(request):
     x,y=stacked_bar.code_logic()
     values_data=[]
     color_values=0
     get_color = ['#FF5733', '#9ACD32', '#0000FF', '#FF69B4', '#8B0000', '#FFA500', '#FFD700', '#008000', '#800080', '#EE82EE', '#2F4F4F',
        '#808080', 'l#4169E1', '#00FF00']
     for k,v in y.items():
       values_data.append({'color':get_color[color_values], 'name':k, 'data':v})
       color_values+=1
     chart  = {
     'chart': {'type': 'column'},
     'title': {'text': 'Matches won of all teams over all the years of IPL'},
     'plotOptions':{'series':{'stacking':'normal'}},
     'xAxis':{'categories': x,'name':'matches',
     'title': {
                       'text': 'Year',
                    #    'y': -10
                    }},
     'yAxis': {'title': {
                       'text': 'Team',
                       'y': -10
                    }},
     'series':values_data
       }
     return render(request,'ipl/index.html',context={'chart':chart})
   

def story_open(request):
    x,y=story.code_logic()

    chart = {
       'chart': {'type': 'column'},
       'title': {'text': ' Win by runs in 2015'},
       'xAxis':{
                'categories': x,

               },
        'yAxis': {
                    # lineWidth: 1,
                    # tickWidth: 1,
                    'title': {
                        # 'align': 'high',
                        # 'offset': 0,
                        'text': 'Runs',
                        # rotation: 0,
                        'y': -10
                        # 'fontSize':'20px'
                    #         'style': {
                    #                 # 'color: '#FF00FF',
                    #                 'fontWeight': 'bold'
                    #             }
                    # },
                    # 'style': {
                    #     'fontWeight': 'bold',
                    # }
                    # 'label': {
                    # 'text': 'Plot band',
                    # 'style': {
                    #     'fontWeight': 'bold',
                    # }
                }    
                },
       'series':[
           {'name':'Winning Team','data':y,'color':'green'}]
    
    }
    return render(request,'ipl/index.html',context={'chart':chart})
